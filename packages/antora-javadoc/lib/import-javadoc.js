'use strict'

const collectBuffer = require('bl')
const expandPath = require('@antora/expand-path-helper')
const { File, MemoryFile, ReadableFile } = require('./file')
const { obj: map } = require('through2')
const ospath = require('path')
const { posix: path } = ospath
const posixify = ospath.sep === '\\' ? (p) => p.replace(/\\/g, '/') : (p) => p
const vzip = require('gulp-vinyl-zip')
const mimeTypes = require('mime-types')
const cheerio = require('cheerio')
const picomatch = require('picomatch')

const RemoteFetch = require('@djencks/antora-remote-fetch')
const PROJECT_NAME = '@djencks/antora-javadoc'

const URI_SCHEME_RX = /^https?:\/\//

const BARE_LAYOUT = 'bare'

const DEFAULT_INCLUDE_PATTERN = '*-search-index.js'

/*
  //Good version, requires https://gitlab.com/antora/antora/-/issues/525
module.exports.register = (pipeline, config = {}) => {
  pipeline.on('afterClassifyContent', (playbook, contentCatalog) => {
    return Promise.all(contentCatalog.getComponents().reduce((cvpromises, component) => {
          component.versions.forEach((componentVersion) => {
            const javadoc = componentVersion.descriptor.javadoc
            processJavadoc(javadoc, config, cvpromises, playbook, contentCatalog, component, componentVersion);
          })
          return cvpromises
        }, [])
    )
  })
}*/

module.exports.register = (pipeline, { config, playbook }) => {
  //caching configuration
  const allowHttp = config.allowHttp || true
  const rejectUnauthorized = 'rejectUnauthorized' in config ? !!config.rejectUnauthorized : true
  const allowedFilePaths = [RemoteFetch.$allPaths]
  const noCache = config.noCache
  const logger = pipeline.require('@antora/logger')(PROJECT_NAME)

  logger.debug({ config })

  const descriptorMap = {}

  pipeline.on('contentAggregated', ({ contentAggregate }) => {
    contentAggregate.forEach((descriptor) => {
      ((descriptor.pipeline || { extensions: [] }).extensions || [])
        .filter((config) => config && (config.require === PROJECT_NAME))
        .forEach((config) => { descriptorMap[key(descriptor)] = config })
    })
    logger.debug({ descriptorMap })
  })

  pipeline.on('contentClassified', ({ contentCatalog }) => {
    //Extract playbook component config
    const mergedDescriptorMap = Object.assign(
      (config.components || []).reduce((accum, componentVersion) => {
        const versionMatcher = picomatcher(componentVersion.version)
        const component = contentCatalog.getComponent(componentVersion.name)
        if (component) {
          component.versions.forEach((version) => {
            const thisVersion = version.version
            if (versionMatcher(thisVersion)) {
              const configCopy = Object.assign({}, componentVersion, { version: thisVersion })
              configCopy.sources = componentVersion.sources.map((source) =>
                Object.assign({}, source, { url: source.url.replace(/{version}/g, thisVersion) })
              )
              accum[key({ name: componentVersion.name, version: thisVersion })] = configCopy
            }
          })
        }
        return accum
      }, {}),
      descriptorMap)

    logger.debug({ mergedDescriptorMap })
    //set up cache location
    const startDir = playbook.dir || '~+'
    const { cacheDir, fetch } = playbook.runtime || {}

    const options = {
      allowHttp,
      rejectUnauthorized,
      allowedFilePaths,
    }
    if (!noCache) {
      options.cacheInfo = {
        preferredCacheDir: cacheDir,
        projectDir: PROJECT_NAME,
        startDir,
      }
    }
    const remoteFetch = new RemoteFetch(options)
    return remoteFetch.initialize()
      .then(() => {
        contentCatalog.getComponents().forEach((component) => {
          component.versions.forEach((componentVersion) => {
            const descriptor = { name: component.name, version: componentVersion.version }
            const javadoc = mergedDescriptorMap[key(descriptor)]
            processJavadoc(javadoc, config, playbook, contentCatalog, component, componentVersion)
          })
        })
        return remoteFetch.complete()
      }
      )

    //end of bad version

    function processJavadoc (javadoc, config, playbook, contentCatalog, component, componentVersion) {
      if (javadoc && javadoc.sources && javadoc.sources.length) {
        const sources = javadoc.sources
        javadoc = Object.assign({}, javadoc)
        delete javadoc.sources
        sources.forEach((source) => {
          const sourceConfig = Object.assign({}, config, javadoc, source)
          addJavadoc(sourceConfig, playbook, contentCatalog, component, componentVersion)
        })
      }
    }

    function addJavadoc (source, playbook, contentCatalog, component, version) {
      const bundleUrl = isUrl(source.url) ? source.url : `file://${expandPath(source.url, { dot: startDir })}`
      const module = source.module || 'javadoc'
      const topic = source.topic || ''
      const layout = source.layout || null
      const matcher = picomatcher(source.includes || DEFAULT_INCLUDE_PATTERN)

      remoteFetch.fetchThen(bundleUrl, 'binary', (info) => {
        if (info) {
          return new Promise((resolve, reject) =>
            (new ReadableFile(new MemoryFile(info)))
              .pipe(vzip.src()
                .on('error', (err) => {
                  err.message = `Invalid JavaDoc jar: '${info.path}': not a valid zip file; ${err.message}`
                  err.info = info
                  reject(err)
                }))
              .pipe(selectFiles(matcher))
              .pipe(bufferizeContents())
              .on('error', reject)
              .pipe(addToContentCatalog(resolve))
          )
        } else {
          return null
        }

        function addToContentCatalog (done) {
          return map((file, _, next) => {
            file.src = expandPageSrc({
              component: component.name,
              version: version.version,
              module,
              family: 'page',
              relative: topic ? topic + '/' + file.path : file.path,
            }, bundleUrl)
            // See https://gitlab.com/antora/antora/-/issues/608 as to whether this should be here at all
            file.asciidoc = { attributes: computePageAttrs(file.src, component, version, module, file.src.extname === '.html' ? layout : BARE_LAYOUT) }
            // We need this attribute in the UI to find the index .js files:
            const depth = file.path.split('/').length - 1
            file.asciidoc.attributes['page-javadoc-root'] = calculateRootPath(depth)
            if (source.extract && file.src.extname === '.html') {
              extract(file, source.extract)
            }
            file = contentCatalog.addFile(file)
            logger.debug({ added: file.src })
            next()
          },
          () => done(contentCatalog))
        }
      }, source.snapshot && fetch)
    }
  })

  function extract (file, element) {
    const doc = cheerio.load(file.contents)
    const extract = doc(element).html()
    if (extract) {
      file.contents = Buffer.from(extract)
    } else {
      logger.warn(`Could not extract ${element} from ${file.src.relative}`)
    }
  }
}

function key (descriptor) {
  return `${descriptor.version}@${descriptor.name}`
}

function calculateRootPath (depth) {
  return depth
    ? Array(depth)
      .fill('..')
      .join('/')
    : '.'
}

function expandPageSrc (src, url) {
  src.basename = path.basename(src.relative)
  src.extname = path.extname(src.relative)
  src.stem = path.basename(src.relative, src.extname)
  src.mediaType = mimeTypes.lookup(src.extname)
  src.origin = { type: 'javadoc', url }
  return src
}

function computePageAttrs (fileSrc, component, version, module, layout) {
  // QUESTION should we soft set the page-id attribute?
  return {
    'page-layout': layout,
    'page-component-name': component.name,
    'page-component-version': version.version,
    'page-version': version.version,
    'page-component-display-version': version.displayVersion,
    'page-component-title': component.title,
    'page-module': module,
    'page-relative': fileSrc.relative,
    'page-origin-type': fileSrc.origin.type,
    'page-origin-url': fileSrc.origin.url,
    // 'doctitle': ???
  }
}

function isUrl (string) {
  return ~string.indexOf('://') && URI_SCHEME_RX.test(string)
}

function selectFiles (matcher) {
  return map((file, _, next) => {
    if (file.isNull()) {
      next()
    } else {
      const path = posixify(file.path)
      const extname = ospath.extname(path)
      if (extname === '.html' || matcher(path)) {
        next(
          null,
          new File({ path, contents: file.contents, stat: file.stat })
        )
      } else {
        next()
      }
    }
  })
}

function bufferizeContents () {
  return map((file, _, next) => {
    // NOTE gulp-vinyl-zip automatically converts the contents of an empty file to a Buffer
    if (file.isStream()) {
      file.contents.pipe(
        collectBuffer((err, data) => {
          if (err) return next(err)
          file.contents = data
          next(null, file)
        })
      )
    } else {
      next(null, file)
    }
  })
}

function picomatcher (patterns) {
  const [include, exclude] = patterns.split(',')
    .map((pattern) => pattern.trim())
    .reduce((accum, pattern) => {
      const [include, exclude] = accum
      pattern.startsWith('!') ? exclude.push(pattern.slice(1)) : include.push(pattern)
      return accum
    }, [[], []])
  const options = exclude.length ? { ignore: exclude } : undefined
  return picomatch(include, options)
}
